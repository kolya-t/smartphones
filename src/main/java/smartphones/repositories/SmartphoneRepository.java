package smartphones.repositories;

import org.springframework.data.repository.CrudRepository;
import smartphones.model.Smartphone;

public interface SmartphoneRepository extends CrudRepository<Smartphone, Long> {
}
