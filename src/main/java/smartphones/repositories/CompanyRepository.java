package smartphones.repositories;

import org.springframework.data.repository.CrudRepository;
import smartphones.model.Company;

public interface CompanyRepository extends CrudRepository<Company, Long> {
    Company findByName(String name);
}
