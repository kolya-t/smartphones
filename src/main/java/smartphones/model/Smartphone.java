package smartphones.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.time.Year;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Smartphone extends AbstractPersistable<Long> {
    private String imei;
    private Float diagonal;
    @ManyToOne
    private Company company;
    private String model;
    private Integer memory;
    @Enumerated
    private Color color;
    private Integer year;
    private Boolean lteSupport;
}
