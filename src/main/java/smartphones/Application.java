package smartphones;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import smartphones.model.Color;
import smartphones.model.Company;
import smartphones.model.Smartphone;
import smartphones.repositories.CompanyRepository;
import smartphones.repositories.SmartphoneRepository;

import java.util.stream.Stream;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner initialize(
            CompanyRepository companyRepository,
            SmartphoneRepository smartphoneRepository
    ) {
        return args -> {
            Stream.of("Apple", "Samsung", "Xiaomi")
                    .map(Company::new)
                    .forEach(companyRepository::save);

            Stream.of(
                    new Smartphone(
                            "185840829168396",
                            5.8f,
                            companyRepository.findByName("Apple"),
                            "iPhone X",
                            128,
                            Color.BLACK,
                            2018,
                            true
                    ),
                    new Smartphone(
                            "739065894864124",
                            5.5f,
                            companyRepository.findByName("Samsung"),
                            "Galaxy S9",
                            64,
                            Color.GRAY,
                            2018,
                            true
                    ),
                    new Smartphone(
                            "673973082973748",
                            4f,
                            companyRepository.findByName("Xiaomi"),
                            "A140",
                            8,
                            Color.BLACK,
                            2017,
                            false
                    )
            ).forEach(smartphoneRepository::save);
        };
    }
}
